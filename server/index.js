var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var people = 0;
var path = __dirname + '/../';

connections = [];
users = [];
rooms = [{users: ['test1', 'test2'], name: 'test1Title'}, {users: ['test3', 'test4'], name: 'test2Title'}];

app.use("/assets", express.static(path + "client/assets"))

app.get('/', function(req, res){
    res.sendFile('client/index.html', { root: path });
});

io.on('connection', function(socket){
  connections.push(socket);
  console.log('Connected: %s sockets connected', connections.length);

  socket.on('disconnect', function(data){
    users.splice(users.indexOf(socket.username), 1);
    updateUsernames();
    connections.splice(connections.indexOf(socket), 1);
    console.log('Disconnected: %s sockets connected', connections.length);
  });

  socket.on('send message', function(data) {
    console.log(data);
    io.emit('new message', {msg: data.msg, room: data.room, user: socket.username});
  });

  socket.on('new user', function(data, callback) {
    callback(true);
    socket.username = data;
    users.push(socket.username);
    updateUsernames();
    updateRooms();
  });

  socket.on('new room', function(data, callback) {
    callback(true);
    socket.rooms = data;
    rooms.push(socket.rooms);
    updateRooms();
  });

  socket.on('join room', function(data, callback) {
    callback(true);
    var room = rooms.find(function(d) {
      return d.name === data;
    });
    room.users.push(socket.username);
    updateRooms();
    updateUsersInRoom(data);
  });

  socket.on('exit room', function(data, callback) {
    callback(true);
    var room = rooms.find(function(d) {
      return d.name === data;
    });
    room.users.splice(room.users.indexOf(socket.username), 1);
    if (room.users.length > 0)
      updateUsersInRoom(data);
    else
      rooms.splice(rooms.indexOf(room), 1);
    updateRooms();
  });

  socket.on('create room', function(data, callback) {
    callback(true);
    var room = {}
    room.users = [socket.username];
    room.name = data;
    rooms.push(room);
    updateRooms();
    updateUsersInRoom(data);
  });

  function updateUsernames() {
    io.emit('get users', users);
  }

  function updateUsersInRoom(val) {
    io.emit('get room', rooms);
  }

  function updateRooms() {
    io.emit('get rooms', rooms);
  }
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
