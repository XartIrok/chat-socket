

socket.on('new message', function(data){
  $('#chat[data-name-room="'+data.room+'"] #messages').append($('<li>').text(data.user+': '+data.msg));
});

socket.on('get room', function(data) {
  var val = $('#chat').attr('data-name-room');
  var room = data.find(function(d) {
    return d.name === val;
  });
  var html = '';
  for (i = 0; i < room.users.length; i++) {
    html += '<li>'+room.users[i]+'</li>';
  }
  $('#users').html(html);
});

socket.on('get rooms', function(data) {
  var html = '';
  for (i = 0; i < data.length; i++) {
    var userRoom = '';
    for (j = 0; j < data[i].users.length; j++) {
      userRoom += data[i].users[j] + ', ';
    }
    userRoom = userRoom.slice(0, -2);
    html += '<div class="col-2">'+data[i].name+'</div>'+
      '<div class="col-8">' + userRoom + '</div>'+
      '<div class="col-2">'+
        '<a href="#" class="click join" data-name-room="'+data[i].name+'">JOIN</a>'+
      '</div>';
  }
  $('#areaRooms').html(html);
})
