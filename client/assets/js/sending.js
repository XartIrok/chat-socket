
$('#areaMessage').submit(function(){
  socket.emit('send message', {msg: $('#m').val(), room: $('#chat').attr('data-name-room')});
  $('#m').val('');
  return false;
});

$('#areaUser').submit(function(){
  socket.emit('new user', $('#u').val(), function(data) {
    if (data) {
      $('#user').hide();
      $('#room').show();
    }
  });
  $('#u').val('');
  return false;
});

$('#areaRooms').on('click', '.join', function() {
  var btn = this;
  socket.emit('join room', $(btn).attr('data-name-room'), function(data) {
    if (data) {
      $('#room').hide();
      $('#chat').show();
      $('#chat').attr('data-name-room', $(btn).attr('data-name-room'));
    }
  });
  return false;
});

$('#chat').on('click', '.exit', function() {
  var btn = this;
  socket.emit('exit room', $('#chat').attr('data-name-room'), function(data) {
    if (data) {
      $('#chat').hide();
      $('#room').show();
      $('#chat').attr('data-name-room', '');
    }
  });
  return false;
});

$('#areaRoom').submit(function() {
  socket.emit('create room', $('#r').val(), function(data) {
    if (data) {
      $('#room').hide();
      $('#chat').show();
      $('#chat').attr('data-name-room', $('#r').val());
    }
  });
  $('#r').val('');
  return false;
});
